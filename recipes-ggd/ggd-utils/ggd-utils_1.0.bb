SUMMARY = "GGD device utils"
LICENSE = "GPL"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b234ee4d69f5fce4486a80fdaf4a4263"

SRC_URI = "git://bitbucket.org/alnicko/facedetect.git;protocol=https"
SRCREV = "18e30d80933ddd4b3fecf091ec152056ae931fbb"

S = "${WORKDIR}/git"

RDEPENDS_${PN} = "bash connman"

FILES_${PN} = "${bindir}/* ${datadir}/${PN}"

do_install () {
	install -d ${D}${bindir}
	install -m 0755 ${S}/tools/wifi_config ${D}${bindir}

	install -d ${D}${datadir}/${PN}
	cp -f ${S}/tools/* ${D}${datadir}/${PN}
}
