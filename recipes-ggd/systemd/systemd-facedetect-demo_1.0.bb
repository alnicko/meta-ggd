SUMMARY = "i.MX GPU Configuration for systemd boot mode"
DESCRIPTION = "Use systemd service to implement the former script rc_gpu.S and other scripts"
LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

SRC_URI = " file://facedetect-demo.service"
S = "${WORKDIR}"

RDEPENDS_${PN} = "systemd ggd-facedetect-demo"

do_install () {
    install -d ${D}${sysconfdir}
    install -d ${D}${systemd_unitdir}/system/
    install -d ${D}${sysconfdir}/systemd/system/multi-user.target.wants/

    install -m 0644 ${WORKDIR}/facedetect-demo.service ${D}${systemd_unitdir}/system

    # Enable the facedetect-demo.service
    ln -sf ${systemd_unitdir}/system/facedetect-demo.service \
            ${D}${sysconfdir}/systemd/system/multi-user.target.wants/facedetect-demo.service
}

FILES_${PN} = "${systemd_unitdir}/system/*.service ${sysconfdir}"

# As this package is tied to systemd, only build it when we're also building systemd.
python () {
    if not bb.utils.contains ('DISTRO_FEATURES', 'systemd', True, False, d):
        raise bb.parse.SkipPackage("'systemd' not in DISTRO_FEATURES")
}
