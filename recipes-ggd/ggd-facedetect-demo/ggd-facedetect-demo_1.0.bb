SUMMARY = "Face detect demo on GGD device"
LICENSE = "GPL"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b234ee4d69f5fce4486a80fdaf4a4263"

SRCBRANCH = "dev"
SRCREV = "release_v1"
SRC_URI = "git://bitbucket.org/alnicko/facedetect.git;protocol=https;branch=${SRCBRANCH}"

S = "${WORKDIR}/git"

DEPENDS = "opencv"
RDEPENDS_${PN} = "opencv"

inherit pkgconfig cmake

FILES_${PN} = "${bindir}/* ${datadir}/${PN}"

do_install () {
	install -d ${D}${bindir}
	install -m 0755 facedetect-demo ${D}${bindir}

	install -d ${D}${datadir}/${PN}/data
	cp -f ${S}/data/* ${D}${datadir}/${PN}/data
}
