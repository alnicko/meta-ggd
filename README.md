# Setup Yocto Project 

Use i.MX Yocto Project User's Guide to setup L4.14.98-2.0.0_ga release.
All documentation can be downloaded from
[https://community.nxp.com/docs/DOC-343277](https://community.nxp.com/docs/DOC-343277)

i.MX Yocto Project User's Guide is in `imx-yocto-L4.14.98_2.0.0_ga.zip` archive.

Currently we support only fsl-imx-fb distro and fsl-image-machine-test image.

**Note:** If your default python interpreter is not v2 you can use
virtual environment (conda or venv) to setup python v2 and use it for
yocto build.

* After successful repo sync, clone meta-ggd layer: 
    ```bash
    cd imx-yocto-bsp
    cd sources
    git clone https://bitbucket.org/alnicko/meta-ggd.git
    cd -
    ```
* Setup build configuration: 
    ```bash
    DISTRO=fsl-imx-fb MACHINE=imx6solo-ggd source fsl-setup-release.sh \
    -b build_fb_imx6solo-ggd
    ```
* Add meta-ggd layer to the conf/bblayers.conf 
    ```bash
    echo "BBLAYERS += \" \${BSPDIR}/sources/meta-ggd \"" >> conf/bblayers.conf
    ```
* Start build:
    ```bash
    bitbake fsl-image-machine-test
    ```
* After successful build uSD card image is the:

  `<BUILD_DIR>/tmp/deploy/images/imx6solo-ggd/fsl-image-machine-test-imx6solo-ggd.sdcard.bz2`

## Adding packages to the Yocto build

1. General

Packages and package groups can be added to image recipes. See the Yocto
Development manual for how to customize an image:
[http://www.yoctoproject.org/docs/current/dev-manual/dev-manual.html#usingpoky-extend-customimage-imagefeatures](http://www.yoctoproject.org/docs/current/dev-manual/dev-manual.html#usingpoky-extend-customimage-imagefeatures)

2. Adding a package to the local build of the BSP:

Search for the corresponding recipe and which layer the recipe is in.
This link is a useful tool for doing so:
[http://layers.openembedded.org/layerindex/branch/morty/layers](http://layers.openembedded.org/layerindex/branch/morty/layers) 

If the package is in the meta-openembedded layer, the recipe is already
available in your build tree.
 
Add the following line to YOCTO_DIR/BUILD_DIR/conf/local.conf:  
```bash
IMAGE_INSTALL_append = " <package>"
```
**Note:** The leading white-space between the " and the package name is
necessary for the append command.

If you need to add a layer to the BSP, clone or extract it to the
`YOCTO_DIR/sources/` directory. Then, modify
`YOCTO_DIR/BUILD_DIR/conf/bblayers.conf` to include this new layer in
`BBLAYERS`:
```
BBLAYERS += "${BSPDIR}/sources/<new_layer>"
```

If there is no recipe for required package in Yocto project you should
create it from scratch.

Here is list of useful manuals how to do that:

[https://wiki.yoctoproject.org/wiki/Building_your_own_recipes_from_first_principles](https://wiki.yoctoproject.org/wiki/Building_your_own_recipes_from_first_principles)

[https://www.wolfssl.com/docs/yocto-openembedded-recipe-guide](https://www.wolfssl.com/docs/yocto-openembedded-recipe-guide)

[https://www.yoctoproject.org/docs/current/dev-manual/dev-manual.html#new-recipe-writing-a-new-recipe](https://www.yoctoproject.org/docs/current/dev-manual/dev-manual.html#new-recipe-writing-a-new-recipe)
