SUMMARY = "MXNET Package"
SECTION = "libs"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=d062b5a0431dc3d14e07b356e09ed27b"

SRC_URI = "https://apache.org/dist/incubator/${PN}/${PV}/apache-${PN}-src-${PV}-incubating.tar.gz"
SRC_URI[sha256sum] = "01eb06069c90f33469c7354946261b0a94824bbaf819fd5d5a7318e8ee596def"

inherit cmake

DEPENDS = "opencv openblas"
RDEPENDS_${PN} = "opencv openblas"

S = "${WORKDIR}/apache-${PN}-src-${PV}-incubating"

OECMAKE_GENERATOR = "Unix Makefiles"

EXTRA_OECMAKE += " -DUSE_SSE=OFF \
	-DUSE_CUDA=OFF \
	-DUSE_JEMALLOC=OFF \
	-DUSE_GPERFTOOLS=OFF \
	-DENABLE_TESTCOVERAGE=OFF \
	-DUSE_OPENCV=ON \
	-DUSE_OPENMP=ON \
	-DUSE_MKL_IF_AVAILABLE=OFF \
	-DUSE_SIGNAL_HANDLER=ON \
	-DUSE_LAPACK=OFF \
"

PACKAGES = "${PN}-dbg ${PN}-staticdev ${PN}-dev ${PN}"

do_compile_prepend () {
	find ${S}/../build -name flags.* -type f -exec sed -i -e 's/-isystem/-I/g' {} \;
}

FILES_${PN} = "${libdir}/*.so ${datadir}/mxnet/libmxnet.so"
FILES_${PN}-dev = "${includedir}/* ${libdir}/cmake/*"
INSANE_SKIP_${PN} += "dev-so"
