DESCRIPTION = "OpenBLAS is an optimized BLAS library based on GotoBLAS2 1.13 BSD version."
SUMMARY = "OpenBLAS : An optimized BLAS library"
AUTHOR = "Alexander Leiva <norxander@gmail.com>"
HOMEPAGE = "http://www.openblas.net/"
SECTION = "libs"
LICENSE = "BSD-3-Clause"

DEPENDS = "make-native"

inherit native

LIC_FILES_CHKSUM = "file://LICENSE;md5=5adf4792c949a00013ce25d476a2abc0"

SRC_URI = "https://github.com/xianyi/OpenBLAS/archive/v${PV}.tar.gz"
SRC_URI[sha256sum] = "17d4677264dfbc4433e97076220adc79b050e4f8a083ea3f853a53af253bc380"

S = "${WORKDIR}/OpenBLAS-${PV}"

do_compile () {
	oe_runmake PREFIX=${exec_prefix} FC=/usr/bin/gfortran
}

do_install() {
	oe_runmake PREFIX=${exec_prefix} DESTDIR=${D} install
	rm -rf ${D}${bindir}
	rm -rf ${D}${libdir}/cmake
}

FILES_${PN}     = "${libdir}/*"
FILES_${PN}-dev = "${includedir} ${libdir}/lib${PN}.so"
