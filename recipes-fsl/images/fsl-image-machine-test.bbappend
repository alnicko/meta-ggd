IMAGE_INSTALL_append = " packagegroup-core-ssh-openssh openssh-sftp-server"
IMAGE_INSTALL_append += " opencv ggd-utils"
IMAGE_INSTALL_append += " ggd-facedetect-demo systemd-facedetect-demo"

COMPATIBLE_MACHINE = "(imx6solo-ggd)"
