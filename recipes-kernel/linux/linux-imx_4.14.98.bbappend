FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://bq27xxx-enable.cfg"
SRC_URI += "file://greengrass.cfg"
SRC_URI += "file://xen-disable.cfg"
SRC_URI += "file://add-imx6solo-ggd-dts.patch"

KERNEL_DEFCONFIG = "${S}/arch/arm/configs/imx_v7_defconfig"
DELTA_KERNEL_DEFCONFIG = "bq27xxx-enable.cfg greengrass.cfg xen-disable.cfg"

do_merge_delta_config[dirs] = "${B}"

do_merge_delta_config() {
    # allow getting KERNEL_DEFCONFIG from outside of the kernel source tree
    cp ${KERNEL_DEFCONFIG} ${S}/arch/${ARCH}/configs/yocto_defconfig

    # create .config with make config
    oe_runmake -C ${S} O=${B} yocto_defconfig

    # add config fragments
    for deltacfg in ${DELTA_KERNEL_DEFCONFIG}; do
        if [ -f "${S}/arch/${ARCH}/configs/${deltacfg}" ]; then
            oe_runmake -C ${S} O=${B} ${deltacfg}
        elif [ -f "${WORKDIR}/${deltacfg}" ]; then
            ${S}/scripts/kconfig/merge_config.sh -m .config ${WORKDIR}/${deltacfg}
        elif [ -f "${deltacfg}" ]; then
            ${S}/scripts/kconfig/merge_config.sh -m .config ${deltacfg}
        fi
    done
    cp .config ${WORKDIR}/defconfig
}
addtask merge_delta_config before do_preconfigure after do_patch

COMPATIBLE_MACHINE = "(imx6solo-ggd)"
