
UBOOT_SRC = "git://bitbucket.org/alnicko/u-boot-imx.git;protocol=https"
SRCBRANCH = "ggd"
SRCREV = "35e4a9f4d79201e3dfa1ff32efc32cef9569309f"

COMPATIBLE_MACHINE = "(imx6solo-ggd)"
