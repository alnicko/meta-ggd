SUMMARY = "Joblib is a set of tools to provide lightweight pipelining in Python."
HOMEPAGE = "https://joblib.readthedocs.io/en/latest/"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=32b289008fb813a27c9025f02b59d03d"

SRC_URI = "https://files.pythonhosted.org/packages/77/c4/26ba5eb6f494d2c307b74ee9bc591bc8153ec4c4fb2a54e780973526cfb5/joblib-${PV}.tar.gz"

SRC_URI[sha256sum] = "0630eea4f5664c463f23fbf5dcfc54a2bc6168902719fa8e19daf033022786c8"

S = "${WORKDIR}/joblib-${PV}"

CLEANBROKEN = "1"

RDEPENDS_${PN} += "\
    python3-core \
"

inherit setuptools3
