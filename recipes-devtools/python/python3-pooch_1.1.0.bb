SUMMARY = "Pooch manages your Python library's sample data files: it automatically downloads and stores them in a local directory, with support for versioning and corruption checks."
HOMEPAGE = "https://github.com/fatiando/pooch"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=34edf66cdaea1acc8b8a430af829c7dc"

SRC_URI = "https://files.pythonhosted.org/packages/05/ff/13cc4168fb24d2f34f7469d8e2e10c46cb2dccb6a9764b3eddab06de096d/pooch-${PV}.tar.gz"

SRC_URI[sha256sum] = "b29687cd3a553d96d73c1fcbaa7551ff5b02a5503bdcd0fa1b1c96d18049d2c3"

S = "${WORKDIR}/pooch-${PV}"

CLEANBROKEN = "1"

RDEPENDS_${PN} += "\
    python3-appdirs \
    python3-packaging \
    python3-requests \
    python3-pytest \
"

inherit setuptools3
