SUMMARY = "SciPy: Scientific Library for Python"
HOMEPAGE = "https://www.scipy.org"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=011ccf01b7e0590d9435a864fc6a4d2b"

SRC_URI = "https://files.pythonhosted.org/packages/04/ab/e2eb3e3f90b9363040a3d885ccc5c79fe20c5b8a3caa8fe3bf47ff653260/scipy-${PV}.tar.gz \
           file://fix-pthread_atfork-undefined-symbol.patch \
"
SRC_URI_append_class-native = " file://fix-native-build.patch"

SRC_URI[md5sum] = "3a97689656f33f67614000459ec08585"
SRC_URI[sha256sum] = "dee1bbf3a6c8f73b6b218cb28eed8dd13347ea2f87d572ce19b289d6fd3fbc59"

S = "${WORKDIR}/scipy-${PV}"

RDEPENDS_${PN} = "${PYTHON_PN}-numpy ${PYTHON_PN}-multiprocessing openblas"
DEPENDS = "${PYTHON_PN}-numpy-native ${PYTHON_PN}-numpy openblas libgfortran"

CLEANBROKEN = "1"

inherit setuptools3

export BLAS = "${STAGING_LIBDIR}/libopenblas.so"
export LAPACK = "${STAGING_LIBDIR}/libopenblas.so"

F90_class-native = "/usr/bin/gfortran"
F90_class-target = "${TARGET_PREFIX}gfortran"
export F90

FC_class-native = "/usr/bin/gfortran"
export FC

export F77FLAGS = "${TARGET_CC_ARCH}"
export F90FLAGS = "${TARGET_CC_ARCH}"

# Numpy expects the LDSHARED env variable to point to a single
# executable, but OE sets it to include some flags as well. So we split
# the existing LDSHARED variable into the base executable and flags, and
# prepend the flags into LDFLAGS
LDFLAGS_prepend_class-target := "${@" ".join(d.getVar('LDSHARED', True).split()[1:])} "

LDSHARED_class-target := "${@d.getVar('LDSHARED', True).split()[0]}"
LDSHARED_class-native := "gcc"
export LDSHARED

# Tell Numpy to look in target sysroot site-packages directory for libraries
LDFLAGS_append = " -L${STAGING_LIBDIR}/${PYTHON_DIR}/site-packages/numpy/core/lib"

# Fix RPATH
# https://www.yoctoproject.org/pipermail/yocto/2016-November/032926.html
do_install_append () {
	find "${D}" -iname "*.so" -exec chrpath -d {} \;
}

RDEPENDS_${PN}_class-native = ""
DEPENDS_class-native = "${PYTHON_PN}-numpy openblas"

BBCLASSEXTEND = "native nativesdk"
