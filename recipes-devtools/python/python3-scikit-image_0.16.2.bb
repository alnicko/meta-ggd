SUMMARY = "scikit-image is a collection of algorithms for image processing."
HOMEPAGE = "https://scikit-image.org/"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=d1a1337f561f551caec1b72eb6e3d91a"

SRC_URI = "https://files.pythonhosted.org/packages/07/ed/58a5157aa484c6aa4e33d4190fa235ce0c4a78010ddf592af4fc257b539f/scikit-image-${PV}.tar.gz"

SRC_URI[sha256sum] = "dd7fbd32da74d4e9967dc15845f731f16e7966cee61f5dc0e12e2abb1305068c"

S = "${WORKDIR}/scikit-image-${PV}"

RDEPENDS_${PN} = " \
	${PYTHON_PN}-numpy \
	${PYTHON_PN}-scipy \
	${PYTHON_PN}-matplotlib \
	${PYTHON_PN}-networkx \
	${PYTHON_PN}-pillow \
	${PYTHON_PN}-imageio \
	${PYTHON_PN}-tifffile \
	${PYTHON_PN}-pywavelets \
	${PYTHON_PN}-pooch \
"
DEPENDS = "${PYTHON_PN}-numpy-native ${PYTHON_PN}-numpy ${PYTHON_PN}-scipy"

CLEANBROKEN = "1"

inherit setuptools3
