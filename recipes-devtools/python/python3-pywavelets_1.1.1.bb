SUMMARY = "PyWavelets, wavelet transform module."
HOMEPAGE = "https://github.com/PyWavelets/pywt"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=4aa8073ec821b7a74c61c7c52c488ebb"

SRC_URI = "https://files.pythonhosted.org/packages/17/6b/ef989cfb3acff2ea966c5f28a7443ccaec2ee136675dfa0366db2635f78a/PyWavelets-${PV}.tar.gz"

SRC_URI[sha256sum] = "1a64b40f6acb4ffbaccce0545d7fc641744f95351f62e4c6aaa40549326008c9"

S = "${WORKDIR}/PyWavelets-${PV}"

CLEANBROKEN = "1"

DEPENDS = "${PYTHON_PN}-numpy-native ${PYTHON_PN}-numpy ${PYTHON_PN}-cython-native"
RDEPENDS_${PN} += "\
    python3-numpy \
"

inherit setuptools3
