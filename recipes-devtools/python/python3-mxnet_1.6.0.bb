SUMMARY = "MXNET python bindings"
SECTION = "libs"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://../LICENSE;md5=d062b5a0431dc3d14e07b356e09ed27b"

SRC_URI = "https://apache.org/dist/incubator/mxnet/${PV}/apache-mxnet-src-${PV}-incubating.tar.gz \
           file://0001-add-usrlib-to-search-path.patch \
"
SRC_URI[sha256sum] = "01eb06069c90f33469c7354946261b0a94824bbaf819fd5d5a7318e8ee596def"

DEPENDS = "${PYTHON_PN}-numpy ${PYTHON_PN}-requests mxnet"
RDEPENDS_${PN} = "${PYTHON_PN}-numpy ${PYTHON_PN}-requests mxnet"

S = "${WORKDIR}/apache-mxnet-src-${PV}-incubating/python"

export MXNET_LIBRARY_PATH = "${STAGING_LIBDIR}/libmxnet.so"

CLEANBROKEN = "1"

inherit setuptools3

do_install_append () {
	# It is just a copy
	rm -rf ${D}/${datadir}
}
