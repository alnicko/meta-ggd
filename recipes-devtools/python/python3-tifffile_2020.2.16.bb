SUMMARY = "Read and write TIFF(r) files."
HOMEPAGE = "https://www.lfd.uci.edu/~gohlke/"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=8bc4a9a5e6864a92bb12801158811da1"

SRC_URI = "https://files.pythonhosted.org/packages/48/be/fee0871b84e17ca1e7ca67fd74328f8eecf04f58bb52b9f17dd19d64f9bc/tifffile-${PV}.tar.gz"

SRC_URI[sha256sum] = "f03b03c4863d232e746859ebdf36e3dfd974364d9944d884bc82b430b0d7290d"

S = "${WORKDIR}/tifffile-${PV}"

CLEANBROKEN = "1"

DEPENDS = "${PYTHON_PN}-numpy-native ${PYTHON_PN}-numpy ${PYTHON_PN}-cython-native"
RDEPENDS_${PN} += "\
    python3-core \
    python3-numpy \
    python3-matplotlib \
"

inherit setuptools3
