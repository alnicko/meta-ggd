SUMMARY = "scikit-learn is an open source machine learning library that supports supervised and unsupervised learning. It also provides various tools for model fitting, data preprocessing, model selection and evaluation, and many other utilities."
HOMEPAGE = "https://scikit-learn.org"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://COPYING;md5=5f6fa516b7dec5faf1c6031236caebce"

SRC_URI = "https://files.pythonhosted.org/packages/e4/40/8bc77d8f536be0a892b37fff19fd81f15935e24724303480f85238ec7f22/scikit-learn-${PV}.post1.tar.gz \
           file://0001-fix-cross-compile.patch \
"

SRC_URI[md5sum] = "4c8d2ab712bd03e01bc55291e1f7bc6e"
SRC_URI[sha256sum] = "57538d138ba54407d21e27c306735cbd42a6aae0df6a5a30c7a6edde46b0017d"

S = "${WORKDIR}/scikit-learn-${PV}.post1"

RDEPENDS_${PN} = "${PYTHON_PN}-numpy ${PYTHON_PN}-scipy ${PYTHON_PN}-joblib"
DEPENDS = "${PYTHON_PN}-numpy-native ${PYTHON_PN}-scipy-native ${PYTHON_PN}-numpy ${PYTHON_PN}-scipy"

CLEANBROKEN = "1"

export SKLEARN_FORCE_HAS_OPENMP = "true"

inherit setuptools3
